package com.nullpointerex.structures;

import javax.swing.JFrame;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

public class dibujo extends JFrame {

	private static final long serialVersionUID = 1L;
    private int cont = 0;
    private final Point[] puntos = new Point[4000];

   public dibujo()
   {
      super("Área de Dibujo");
      
      Container ventana = getContentPane();
      ventana.setLayout(new FlowLayout());

      addMouseMotionListener(new MouseMotionAdapter() {
		   public void mouseDragged(MouseEvent e)
            {
               if(cont<puntos.length){
                  puntos[cont]=e.getPoint();
                  cont++;
                  repaint(125,75,175,200);
               }
            }
         });

      setResizable(false);
      setSize(400,300);  
      setVisible(true);  
   }
	
   public void paint(Graphics g) 
   {
   	
      super.paint(g);
      g.drawRect(125,75,175,200);
      g.setColor(Color.BLUE);

      for (int i = 0; i < puntos.length && puntos[i]!=null; i++ )
         g.fillOval(puntos[i].x,puntos[i].y, 7, 7);		
   }

}