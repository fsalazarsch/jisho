package com.nullpointerex.structures;

import com.nullpointerex.models.kana;
import com.nullpointerex.models.kanji;
import com.nullpointerex.models.noken;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class funciones {

    public static ArrayList<kana> hiragana = new ArrayList<>();
    public static ArrayList<kana> katakana = new ArrayList<>();
    public static ArrayList<kanji> kan_ji= new ArrayList<>();
    public static ArrayList<noken> nk5 = new ArrayList<>();
    public static ArrayList<noken> nk4 = new ArrayList<>();
    public static ArrayList<noken> nk3 = new ArrayList<>();
    public static ArrayList<noken> nk2 = new ArrayList<>();


    public void setkana(String src){
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader("src/com/nullpointerex/src/"+src+".json"))
        {
            Object obj = jsonParser.parse(reader);
            JSONArray kanaList = (JSONArray) obj;
            if (src.equals("hiragana"))
                kanaList.forEach( kan -> parseKanaObject( (JSONObject) kan, hiragana ) );
            if (src.equals("katakana"))
                kanaList.forEach( kan -> parseKanaObject( (JSONObject) kan, katakana ) );

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    private static void parseKanaObject(JSONObject k, ArrayList<kana> silabario)
    {
        JSONObject kanaObject = (JSONObject) k;
        silabario.add(new kana( (String) kanaObject.get("kana"), (String) kanaObject.get("silaba"), (String) kanaObject.get("romaji")));
    }

    public String kana_to_romaji(String kanastring, ArrayList<kana> silabario){


        String romaji = "";
        String diptongos = "";

        if (silabario.equals(hiragana)){
            diptongos = "ゃゅょ";
        }
        if (silabario.equals(katakana)){
            diptongos = "ャュョ";
        }

        for( int i =0; i< kanastring.length(); i++){

            char ch = kanastring.charAt(i);



            try {
                if (diptongos.contains( String.valueOf(kanastring.charAt(i + 1)) )){
                    //((kanastring.charAt(i + 1) == 'ゃ') || (kanastring.charAt(i + 1) == 'ゅ') || (kanastring.charAt(i + 1) == 'ょ')) {
                    String c = String.valueOf(kanastring.charAt(i)) + String.valueOf(kanastring.charAt(i + 1));

                    kana kaux = silabario.stream().filter(p -> p.getKana().equals(c)).findAny().orElse(null);
                    romaji += kaux.getRomaji();
                    i++;
                } else {
                    String c = String.valueOf(ch);

                    try {
                        kana kaux = silabario.stream().filter(p -> p.getKana().equals(c)).findAny().orElse(null);
                        ;
                        romaji += kaux.getRomaji();
                    } catch (NullPointerException ignored) {}
                }
            }catch (Exception npe) {

                String c = String.valueOf(ch);
                try {
                    kana kaux = silabario.stream().filter(p -> p.getKana().equals(c)).findAny().orElse(null);
                    ;
                    romaji += kaux.getRomaji();
                } catch (NullPointerException ignored) {}
            }
        }
        return romaji;
    }

    public void setkanji(Integer grado){
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader("src/com/nullpointerex/src/kanji.json"))
        {
            Object obj = jsonParser.parse(reader);
            JSONArray kanjiList = (JSONArray) obj;
            if (grado == null)
                kanjiList.forEach( kan -> parseKanjiObject( (JSONObject) kan ) );
            else
                kanjiList.forEach( kan -> parseKanjigradoObject( (JSONObject) kan, kan_ji,  String.valueOf(grado) ) );

        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }



    private static void parseKanjiObject(JSONObject k)
    {
        kan_ji.add( new kanji( Short.parseShort( (String) k.get("id_kanji")), (String) k.get("kanji"), (String) k.get("kun"), (String) k.get("on"), (String) k.get("significado"), Short.parseShort((String) k.get("grado"))) );
    }
    private static void parseKanjigradoObject(JSONObject k, ArrayList<kanji> silabario, String grado) {


        if (grado.equals(k.get("grado"))){
            silabario.add(new kanji(
                    new Short((String) k.get("id_kanji")),
                    (String) k.get("kanji"),
                    (String) k.get("kun"),
                    (String) k.get("on"),
                    (String) k.get("significado"),
                    new Short((String) k.get("grado"))
            ));

        }
    }


    public void setnoken(String src){
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader("src/com/nullpointerex/src/"+src+".json"))
        {
            Object obj = jsonParser.parse(reader);
            JSONArray nokenlist = (JSONArray) obj;

            if (src.equals("n5"))
                nokenlist.forEach( kan -> parseNokenObject( (JSONObject) kan, nk5 ) );
            if (src.equals("n4"))
                nokenlist.forEach( kan -> parseNokenObject( (JSONObject) kan, nk4 ) );
            if (src.equals("n3"))
                nokenlist.forEach( kan -> parseNokenObject( (JSONObject) kan, nk3 ) );
            if (src.equals("n2"))
                nokenlist.forEach( kan -> parseNokenObject( (JSONObject) kan, nk2 ) );

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }



    private static void parseNokenObject(JSONObject k,  ArrayList<noken> diccionario)
    {
        JSONObject nokenObject = (JSONObject) k;

        diccionario.add( new noken( (String) nokenObject.get("lectura"),(String) nokenObject.get("Significado"),(String) nokenObject.get("Kanji")));
    }
}
