package com.nullpointerex.structures.quiz;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.GridLayout;


public class set_quiz_kanji extends JFrame {


   public set_quiz_kanji() {
      super("Buscador de Kanji");

      getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
      JPanel panel = new JPanel();

      JPanel grid = new JPanel(new GridLayout(1, 2));
      JLabel label = new JLabel("Quiz");
      String[] nombres = {"Significado", "Kunyomi", "Onyomi"};
      JComboBox bar = new JComboBox(nombres);
      grid.add(label);
      grid.add(bar);

      JPanel grid2 = new JPanel(new GridLayout(1, 2));
      JLabel label2 = new JLabel("Grado");
      String[] nombres2 = {"S", "1", "2", "3", "4", "5", "6", "7"};
      JComboBox bar2 = new JComboBox(nombres2);
      grid2.add(label2);
      grid2.add(bar2);

      JPanel grid3 = new JPanel(new GridLayout(1, 1));
      JButton btn = new JButton("Buscar");
      grid3.add(btn);

      panel.add(grid);
      panel.add(grid2);
      panel.add(btn);

      getContentPane().add(panel);
      setSize(300, 300);
      setVisible(true);
      setResizable(false);

      btn.addActionListener(e -> {
         String tipo_test = "";

         if ( bar.getSelectedIndex() == 0)
            tipo_test = "significado";
         if ( bar.getSelectedIndex() == 1)
            tipo_test = "kun";
         if ( bar.getSelectedIndex() == 2)
            tipo_test = "on";

            kanji_quiz a = new kanji_quiz(bar2.getSelectedIndex(), tipo_test);
            a.setVisible(true);
      });

   }

   public static void main(String[] args) {
      set_quiz_kanji p = new set_quiz_kanji();
      p.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
   }
}

