package com.nullpointerex.structures.quiz;

import com.nullpointerex.models.kana;
import com.nullpointerex.structures.funciones;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class quiz extends JFrame {


   funciones f = new funciones();

   public static ArrayList<kana>  pool = new ArrayList<>();
   private final List<kana> preguntas;

   public short buenas = 0;
   public short malas = 0;

   public int total;

   public quiz(String diccionario)
   {
      super(diccionario.toUpperCase());
      f.setkana(diccionario);

      if (diccionario.equalsIgnoreCase("hiragana")){
         pool = f.hiragana;
         Collections.shuffle(f.hiragana);
      }

      if (diccionario.equalsIgnoreCase("katakana")) {
         pool = f.katakana;
         Collections.shuffle(f.katakana);
      }

      Collections.shuffle(pool);
      getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
      preguntas =  pool;//.subList(10,20);

      total = preguntas.size();

      kana incognita = preguntas.get(new Random().nextInt(preguntas.size()));
      disponer(incognita, diccionario);

   }

   public static void main(String[] args)
   {
      quiz p = new quiz(args[0]);
      p.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
   }

   public void disponer(kana incognita, String diccionario) {

      ArrayList<String> opciones = new ArrayList<>();
      String etiqueta = incognita.getRomaji();
      String respuesta = incognita.getKana();
      opciones.add(incognita.getKana());

      if (diccionario.equals("hiragana")){
         opciones.add(f.hiragana.get(new Random().nextInt(f.hiragana.size())).getKana());
         opciones.add(f.hiragana.get(new Random().nextInt(f.hiragana.size())).getKana());
         opciones.add(f.hiragana.get(new Random().nextInt(f.hiragana.size())).getKana());
         opciones.add(f.hiragana.get(new Random().nextInt(f.hiragana.size())).getKana());
      }

      if (diccionario.equals("katakana")){
         opciones.add(f.katakana.get(new Random().nextInt(f.katakana.size())).getKana());
         opciones.add(f.katakana.get(new Random().nextInt(f.katakana.size())).getKana());
         opciones.add(f.katakana.get(new Random().nextInt(f.katakana.size())).getKana());
         opciones.add(f.katakana.get(new Random().nextInt(f.katakana.size())).getKana());
      }

      JPanel panel = new JPanel(new GridLayout(6, 1));
      getContentPane().removeAll();

      JTextArea label = new JTextArea(etiqueta);
      label.setFont(label.getFont().deriveFont(40f));
      label.setEditable(false);
      label.setOpaque(false);

      panel.add(label);
      Collections.shuffle(opciones);

      for (String item : opciones) {
         JButton btn1 = new JButton("<html><h1>"+item+"</h1></html>");
         panel.add(btn1);

         btn1.addActionListener(e -> {
            if(e.getActionCommand().equals(respuesta)) {
               buenas +=1;
            } else
               malas += 1;

            preguntas.remove(incognita);
            if (preguntas.isEmpty()){
               JOptionPane.showMessageDialog(quiz.this, "Test terminado, tus resultados\n " + buenas + "/" + total + "\n" + buenas * 100 / total + "% de aprobación");
               dispose();
            }
            else{
               disponer( preguntas.get(new Random().nextInt(preguntas.size())), diccionario );
            }
         });
      }

      panel.setAlignmentX(Component.CENTER_ALIGNMENT);
      panel.setPreferredSize(new Dimension(300, 300));
      panel.setMaximumSize(new Dimension(300, 300));

      getContentPane().add(panel);
      setSize(400, 400);
      setVisible(true);
      setResizable(true);
   }
}

