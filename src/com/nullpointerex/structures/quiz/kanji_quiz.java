package com.nullpointerex.structures.quiz;

import com.nullpointerex.models.kanji;
import com.nullpointerex.structures.funciones;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class kanji_quiz extends JFrame {

   public static ArrayList<kanji>  pool = new ArrayList<>();
   private final List<kanji> preguntas;

   public short buenas;
   public short malas;
   public int total;

   funciones f = new funciones();



   public kanji_quiz(Integer grado, String tipo)
   {
      super("kanji");
      f.setkanji(grado);
      pool = f.kan_ji;

      Collections.shuffle(pool);
      getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
      preguntas =  pool;//.subList(10,20);

      total = preguntas.size();


      kanji incognita = preguntas.get(new Random().nextInt(preguntas.size()));
         disponer(incognita, tipo);



   }

   public static void main(String[] args)
   {
      kanji_quiz p = new kanji_quiz(Integer.getInteger(args[0]), args[1]);
      p.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
   }

   public void disponer(kanji incognita, String tipo) {


      ArrayList<String> opciones = new ArrayList<>();
      String etiqueta = incognita.getKanji();
      String respuesta = "";

      if (tipo.equals("kun"))
         respuesta = incognita.getKun();

      if (tipo.equals("on"))
         if (!incognita.getOn().equals(""))
            respuesta = incognita.getOn();
         else
            respuesta = "[no tiene]";

      if (tipo.equals("significado"))
         respuesta = incognita.getSignificado();


      opciones.add(respuesta);

      if (tipo.equals("on"))
         for (int i = 0; i< 4; i++) {
            int val = new Random().nextInt(f.kan_ji.size());
            if (!f.kan_ji.get(val).getOn().equals(""))
               opciones.add(f.kan_ji.get(val).getOn());
            else
               opciones.add("[no tiene]");
         }
      if (tipo.equals("kun"))
         for (int i = 0; i< 4; i++)
            opciones.add(f.kan_ji.get(new Random().nextInt(f.kan_ji.size())).getKun());

      if (tipo.equals("significado"))
         for (int i = 0; i< 4; i++)
            opciones.add(f.kan_ji.get(new Random().nextInt(f.kan_ji.size())).getSignificado());


      JPanel panel = new JPanel(new GridLayout(6, 1));
      getContentPane().removeAll();

      JTextArea label = new JTextArea(etiqueta);
      label.setFont(label.getFont().deriveFont(60f));
      label.setEditable(false);
      label.setOpaque(false);
      panel.add(label);
      Collections.shuffle(opciones);

      for (String item : opciones) {
         JButton btn1 =  new JButton("<html><h3>"+item+"</h3></html>");
         panel.add(btn1);

         final String finalRespuesta = respuesta;
         btn1.addActionListener(e -> {

            if(e.getActionCommand().equals(finalRespuesta)){
               buenas +=1;
            }
            else {
               malas += 1;
            }
            preguntas.remove(incognita);
            if (preguntas.isEmpty()){
               JOptionPane.showMessageDialog(kanji_quiz.this, "Test terminado, tus resultados\n " + buenas + "/" + total + "\n" + buenas * 100 / total + "% de aprobación");
               dispose();
            }
            else{
               disponer( preguntas.get(new Random().nextInt(preguntas.size())), tipo);
            }
         });
      }

      panel.setAlignmentX(Component.CENTER_ALIGNMENT);
      panel.setPreferredSize(new Dimension(300, 400));
      panel.setMaximumSize(new Dimension(300, 400));

      getContentPane().add(panel);
      setSize(400, 500);
      setVisible(true);
      setResizable(false);
   }
}

