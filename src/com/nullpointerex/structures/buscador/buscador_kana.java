package com.nullpointerex.structures.buscador;

import com.nullpointerex.structures.dibujo;
import com.nullpointerex.models.kana;
import com.nullpointerex.structures.funciones;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Arrays;


public class buscador_kana extends JFrame{

   funciones f = new funciones();

   public buscador_kana()
   {
      super("Hiragana / Katakana");

      for (String s : Arrays.asList("hiragana", "katakana")) {
         f.setkana(s);
      }

      getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
      JPanel panel = new JPanel();
      JPanel panel2 = new JPanel();

      JScrollPane scrollFrame = new JScrollPane(panel2);

      
      
      JPanel grid2 = new JPanel(new GridLayout(1, 2));
      JLabel label2 = new JLabel("Escritura");
      JTextField txt2 = new JTextField(10);
      grid2.add(label2);
      grid2.add(txt2);

      JButton btn2 = new JButton("Buscar");
      JButton btn3 = new JButton("Dibujar");


      panel.add(grid2);
      panel.add(btn2);
      panel.add(btn3);

      panel.setAlignmentX(Component.CENTER_ALIGNMENT);
      panel.setPreferredSize(new Dimension(300, 100));
      panel.setMaximumSize(new Dimension(300, 100));
      panel.setBorder(BorderFactory.createTitledBorder("Buscar"));
      getContentPane().add(panel);


      panel2.setAlignmentX(Component.CENTER_ALIGNMENT);
      //panel2.setPreferredSize(new Dimension(600, 300));
      scrollFrame.setPreferredSize(new Dimension( 300,300));

      panel2.setBorder(BorderFactory.createTitledBorder("Resultado"));
      panel2.setAutoscrolls(true);
      getContentPane().add(scrollFrame);

      setSize(300, 350);
      setVisible(true);
      setResizable(false);


      btn2.addActionListener(e -> {

         ArrayList<kana> aux = new ArrayList<kana>();
         aux = f.hiragana;

         String escritura = txt2.getText();
         String silabs[] = new String[2];

         kana kaux = f.hiragana.stream().filter(p -> p.getRomaji().equals(escritura)).findAny().orElse(null);
         silabs[0] = kaux.getKana();

         kaux = f.katakana.stream().filter(p -> p.getRomaji().equals(escritura)).findAny().orElse(null);
         silabs[1] = kaux.getKana();

         String contenido = "<html><table>";
         contenido += "<tr><td><h3>Hiragana</h3></td><td><h3>Katakana</h3></td></tr>";

         panel2.removeAll();
         panel2.setVisible(false);

         for (kana item : aux){
            if (item.getRomaji().equals(escritura)){
               contenido += "<tr><td><h1 style='font-size: 60px'>" + silabs[0] + "</h1></td>";
               contenido += "<td><h1 style='font-size: 60px'>" + silabs[1] + "</h1></td></tr>";
            }
         }
         contenido += "</table></html>";
         JLabel item_content = new JLabel(contenido);
         panel2.add(item_content);
         panel2.setVisible(true);


      });

      btn3.addActionListener(e -> {
         dibujo dib = new dibujo();
         dib.setAlwaysOnTop(true);
      });

   } 
   	
   public static void main( String[] args )
   { 
      buscador_kana p = new buscador_kana();
      p.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      
   }

}

