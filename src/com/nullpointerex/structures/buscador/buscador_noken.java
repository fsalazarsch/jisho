package com.nullpointerex.structures.buscador;

import com.nullpointerex.models.noken;
import com.nullpointerex.structures.funciones;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Arrays;


public class buscador_noken extends JFrame{

   funciones f = new funciones();

   public buscador_noken()
   {
      super("Vocabulario noken");

      for (String s1 : Arrays.asList("n5", "n4", "n3", "n2")) {
         f.setnoken(s1);
      }

      for (String s : Arrays.asList("hiragana", "katakana")) {
         f.setkana(s);
      }

      getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
      JPanel panel = new JPanel();
      JPanel panel2 = new JPanel();

      JScrollPane scrollFrame = new JScrollPane(panel2);


      JPanel grid1 = new JPanel(new GridLayout(1, 2));      
      JLabel label1= new JLabel("Significado");      
      JTextArea txt1 =  new JTextArea(4,10);
      grid1.add(label1);
      grid1.add(txt1);
      
      
      JPanel grid2 = new JPanel(new GridLayout(1, 2));
      JLabel label2 = new JLabel("Escritura");
      JTextField txt2 = new JTextField(10);
      grid2.add(label2);
      grid2.add(txt2);


      JPanel grid3 = new JPanel(new GridLayout(1, 2));
      JLabel label3 = new JLabel("Diccionario");
      String[] nombres = {"Noken 5", "Noken 4", "Noken 3", "Noken 2-1"  };

      JComboBox<String> diccionario = new JComboBox<>(nombres);
      //diccionario.setMaximumRowCount(4);

      grid3.add(label3);
      grid3.add(diccionario);



      JButton btn2 = new JButton("Buscar");


      panel.add(grid1);
      panel.add(grid2);
      panel.add(grid3);
      panel.add(btn2);

      
      
      panel.setAlignmentX(Component.CENTER_ALIGNMENT);
      panel.setPreferredSize(new Dimension(300, 300));
      panel.setMaximumSize(new Dimension(300, 300));
      panel.setBorder(BorderFactory.createTitledBorder("Buscar"));
      getContentPane().add(panel);

      panel2.setAlignmentX(Component.CENTER_ALIGNMENT);
      scrollFrame.setPreferredSize(new Dimension( 600,600));

      panel2.setBorder(BorderFactory.createTitledBorder("Resultado"));
      panel2.setAutoscrolls(true);
      getContentPane().add(scrollFrame);

      setSize(600, 600);
      setVisible(true);
      setResizable(false);


      btn2.addActionListener(e -> {
         ArrayList<noken> aux = new ArrayList<noken>();
         String noken = diccionario.getSelectedItem().toString();
         String escritura = txt2.getText();

         StringBuilder contenido = new StringBuilder("<html><table>");
         contenido.append("<tr><td>Kanji</td><td>Lectura</td><td>Significado</td></tr>");

         panel2.removeAll();
         panel2.setVisible(false);

         if (noken.equals("Noken 5")){
         aux = f.nk5;
         }
         if (noken.equals("Noken 4")){
         aux = f.nk4;
         }
         if (noken.equals("Noken 3")){
         aux = f.nk3;
         }
         if (noken.equals("Noken 2-1")){
         aux = f.nk2;
         }


         for (noken item : aux){
            if (item.getSignificado().contains(txt1.getText()))
               if (f.kana_to_romaji(item.getLectura(), f.katakana).contains(escritura) || (f.kana_to_romaji(item.getLectura(), f.hiragana).contains(escritura))) {
               contenido.append("<tr><td><h3>").append(item.getKanji()).append("</h3></td>");
               contenido.append("<td><h3>").append(item.getLectura()).append("</h3></td>");
               contenido.append("<td><h3>").append(item.getSignificado()).append("</h3></td></tr>");
               }
         }
         contenido.append("</table></html>");
         JLabel item_content = new JLabel(contenido.toString());
         panel2.add(item_content);
         panel2.setVisible(true);

      });

   } 
   	
   public static void main( String[] args )
   { 
      buscador_noken p = new buscador_noken();
      p.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      
   }

}

