package com.nullpointerex.structures.buscador;

import com.nullpointerex.structures.dibujo;
import com.nullpointerex.models.kanji;
import com.nullpointerex.structures.funciones;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;


public class buscador_kanji extends JFrame{

   funciones f = new funciones();


   public buscador_kanji()
   {
      super("Buscador de Kanji");
      f.setkanji(null);
      f.setkana("hiragana");
      f.setkana("katakana");

      getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
      JPanel panel = new JPanel();
      JPanel panel2 = new JPanel();

      JScrollPane scrollFrame = new JScrollPane(panel2);


      JPanel grid1 = new JPanel(new GridLayout(1, 2));      
      JLabel label1= new JLabel("Significado");      
      JTextArea txt1 =  new JTextArea(4,10);
      grid1.add(label1);
      grid1.add(txt1);
      
      
      JPanel grid2 = new JPanel(new GridLayout(1, 2));
      JLabel label2 = new JLabel("Kunyomi");
      JTextField txt2 = new JTextField(10);
      grid2.add(label2);
      grid2.add(txt2);

      
      JPanel grid3 = new JPanel(new GridLayout(1, 2));      
      JLabel label3 = new JLabel("Onyomi");
      JTextField txt3 = new JTextField(10);
      grid3.add(label3);
      grid3.add(txt3);

      
      JPanel grid4 = new JPanel(new GridLayout(1, 2));      
      JLabel label4 = new JLabel("Grado");
      JTextField txt4 = new JTextField(10);

      grid4.add(label4);
      grid4.add(txt4);      
      JButton btn1 = new JButton("Reset");
      JButton btn2 = new JButton("Buscar");
      JButton btn3 = new JButton("Dibujar");


      panel.add(grid1);
      panel.add(grid2);      
      panel.add(grid3);
      panel.add(grid4);
      panel.add(btn1);
      panel.add(btn2);
      panel.add(btn3);

      
      
      panel.setAlignmentX(Component.CENTER_ALIGNMENT);
      panel.setPreferredSize(new Dimension(300, 300));
      panel.setMaximumSize(new Dimension(300, 300));
      panel.setBorder(BorderFactory.createTitledBorder("Buscar"));
      getContentPane().add(panel);

      //JTextArea labelk= new JTextArea();



      panel2.setAlignmentX(Component.CENTER_ALIGNMENT);
      //panel2.setPreferredSize(new Dimension(600, 300));
      scrollFrame.setPreferredSize(new Dimension( 600,600));

      panel2.setBorder(BorderFactory.createTitledBorder("Resultado"));
      panel2.setAutoscrolls(true);
      getContentPane().add(scrollFrame);

      setSize(600, 600);
      setVisible(true);
      setResizable(false);

      btn3.addActionListener(e -> {
         dibujo dib = new dibujo();
         dib.setAlwaysOnTop(true);
      });


      btn2.addActionListener(e -> {
         ArrayList<kanji> aux = new ArrayList<>();

         String contenido = "<html><table>";

         panel2.removeAll();
         panel2.setVisible(false);


         for (kanji item : f.kan_ji){
            if (item.getSignificado().contains(txt1.getText()))
               if (f.kana_to_romaji(item.getKun(), f.katakana).contains(txt2.getText()))
                  if (f.kana_to_romaji(item.getOn(), f.hiragana).contains(txt3.getText()))
                     if ((txt4.getText().equals("")) || ( txt4.getText().equals( String.valueOf(item.getGrado()) ) ))
                     {
                     aux.add(item);

                     contenido += "<tr><td rowspan='3'><h1>"+item.getGrado()+"</h1><h1 style='font-size: 60px'>"+item.getKanji()+"</h1></td>";
                     contenido += "<td><h3>"+item.getSignificado()+"</h3></td></tr><tr>";
                     contenido += "<td><h1>"+item.getKun()+"</h1></td></tr><tr style='border-bottom: 1pt solid black'><td><h1>"+item.getOn()+"</h1></td>";
                     contenido += "</tr>";
                     }
         }
         contenido += "</table></html>";
         JLabel item_content = new JLabel(contenido);
         panel2.add(item_content);
         panel2.setVisible(true);



      });

      btn1.addActionListener( new ActionListener() {
  	    @Override
  	    public void actionPerformed(ActionEvent e) {
    	      txt1.setText("");  
      	      txt2.setText("");  
      	      txt3.setText("");  
      	      txt4.setText("");  
  	    }
    });
      
   } 
   	
   public static void main( String[] args )
   { 
      buscador_kanji p = new buscador_kanji();
      p.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      
   }

}

