package com.nullpointerex.models;

public class kanji{

	private short id_kanji;
	private String kanji;
	private String kun;
	private String on;
	private String significado;
	private short grado;
	
	public kanji(short id_kanji, String kanji, String kun, String on, String significado, short grado){
		this.id_kanji = id_kanji;
		this.kanji = kanji;
		this.kun = kun;
		this.on = on;
		this.significado = significado;
		this.grado = grado;
	}

	public short getId_kanji() {
		return id_kanji;
	}

	public void setId_kanji(short id_kanji) {
		this.id_kanji = id_kanji;
	}

	public String getKanji() {
		return kanji;
	}

	public void setKanji(String kanji) {
		this.kanji = kanji;
	}

	public String getKun() {
		return kun;
	}

	public void setKun(String kun) {
		this.kun = kun;
	}

	public String getOn() {
		return on;
	}

	public void setOn(String on) {
		this.on = on;
	}

	public String getSignificado() {
		return significado;
	}

	public void setSignificado(String significado) {
		this.significado = significado;
	}

	public short getGrado() {
		return grado;
	}

	public void setGrado(short grado) {
		this.grado = grado;
	}

	@Override
	public String toString() {
		return "" +
				"kanji{" +
				"id_kanji=" + id_kanji +
				", kanji='" + kanji + '\'' +
				", kun='" + kun + '\'' +
				", on='" + on + '\'' +
				", significado='" + significado + '\'' +
				", grado=" + grado +
				'}';
	}
}