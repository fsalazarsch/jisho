package com.nullpointerex.models;

public class kana {

	private String kana;
	private String silaba;
	private String romaji;
	
	public kana(String kana, String silaba, String romaji){
		this.kana = kana;
		this.silaba = silaba;
		this.romaji = romaji;
	}

	public String getKana() {
		return kana;
	}
	public void setKana(String kana) {
		this.kana = kana;
	}
	public String getSilaba() {
		return silaba;
	}
	public void setSilaba(String silaba) {

		this.silaba = silaba;
	}
	public String getRomaji() {
		return romaji;
	}
	public void setRomaji(String romaji) {
		this.romaji = romaji;
	}
	
	
}
