package com.nullpointerex.models;

public class noken {

	private String lectura;
	private String significado;
	private String kanji;

	public noken(String lectura, String significado, String kanji){
		this.lectura = lectura;
		this.significado = significado;
		this.kanji = kanji;
	}

	public String getLectura() {

		return lectura;
	}
	public void setLectura(String lectura) {

		this.lectura = lectura;
	}
	public String getSignificado() {

		return significado;
	}
	public void setSignificado(String significado) {

		this.significado = significado;
	}

	public String getKanji() {

		return kanji;
	}
	public void setKanji(String kanji) {

		this.kanji = kanji;
	}


}
