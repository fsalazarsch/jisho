package com.nullpointerex;

import com.nullpointerex.structures.buscador.buscador_kana;
import com.nullpointerex.structures.buscador.buscador_kanji;
import com.nullpointerex.structures.buscador.buscador_noken;
import com.nullpointerex.structures.quiz.quiz;
import com.nullpointerex.structures.quiz.set_quiz_kanji;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import java.awt.Container;
import java.awt.FlowLayout;
import java.util.Arrays;

class Main extends JFrame {

   public Main()
   {
   	//titulo
      super( "Teru-teru" );
         
      Container menu = getContentPane();
      menu.setLayout(new FlowLayout());

	
      //creacion del barmenu
      JMenuBar menu_p = new JMenuBar();
      
      JMenu menubuscar = new JMenu("Buscar");
      JMenuItem mbkana = new JMenuItem("Silabarios");
      JMenuItem mbkanji = new JMenuItem("Kanji");
      JMenuItem mbnoken = new JMenuItem("Noken");

       for (JMenuItem jMenuItem : Arrays.asList(mbkana, mbkanji, mbnoken)) {
           menubuscar.add(jMenuItem);
       }

       JMenu Ayuda = new JMenu("Ayuda");
       JMenuItem f1 = new JMenuItem("Ayuda...");
       JMenuItem cre = new JMenuItem("Creditos");

       for (JMenuItem jMenuItem : Arrays.asList(f1, cre)) {
           Ayuda.add(jMenuItem);
       }

       JMenu menuquiz= new JMenu("Quiz");
       JMenuItem mqkanji = new JMenuItem("Kanji");
       JMenu mqkana = new JMenu("Kana");

       menuquiz.add(mqkanji);
       menuquiz.add(mqkana);

       JMenuItem qhirag = new JMenuItem("Hiragana");
       JMenuItem qkatak = new JMenuItem("Katakana");

       for (JMenuItem jMenuItem : Arrays.asList(qhirag, qkatak)) {
           mqkana.add(jMenuItem);
       }

       menu_p.add(menubuscar);
       menu_p.add(menuquiz);
       menu_p.add(Ayuda);

       setJMenuBar(menu_p);

       Icon fondo = new ImageIcon("src/com/nullpointerex/src/teruteru.png");
       JLabel label = new JLabel();
       label.setIcon(fondo);
       menu.add(label);

       mbkana.addActionListener(e -> {
           buscador_kana b = new buscador_kana();
           b.setVisible(true);
       });

       mbkanji.addActionListener(e -> {
           buscador_kanji b = new buscador_kanji();
           b.setVisible(true);
	   });

       mbnoken.addActionListener(e -> {
           buscador_noken b = new buscador_noken();
           b.setVisible(true);
       });

       qhirag.addActionListener(e -> {
           quiz a = new quiz("hiragana");
           a.setVisible(true);
       });

       qkatak.addActionListener(e -> {
           quiz a = new quiz("katakana");
           a.setVisible(true);
       });

       mqkanji.addActionListener(e -> {
           set_quiz_kanji a = new set_quiz_kanji();
           a.setVisible(true);
       });

       f1.addActionListener(e -> JOptionPane.showMessageDialog( Main.this,"Buscar: Busca y practica escritura de acuerdo a su nivel\nQuiz: Quiz de acuerdo a los parametros escogidos\n"));
       cre.addActionListener(e -> JOptionPane.showMessageDialog(Main.this, "Hecho por: fsalazarsch\nPermitida su copia parcial y/o total\n"));

       setResizable(false);
       setSize(600, 500);
       setVisible(true);
   }

   public static void main(String[] args)
   { 
      Main m = new Main();
      m.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
   }

}